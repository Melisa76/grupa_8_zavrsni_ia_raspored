package START.PROFESOR.obrisi_rezervaciju;

import START.ENTITETI.rezervacija;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.sql.Time;

import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import javax.swing.JOptionPane;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

import static java.lang.Integer.parseInt;



public class obrisi_rezervacijuController implements Initializable {

    @FXML private TableView<rezervacija> table_rezervacija;

    @FXML private TableColumn<rezervacija, Integer> idRezervacija;
    @FXML private TableColumn<rezervacija, String> nazivRezervacija;
    @FXML private TableColumn<rezervacija, String> datumRezervacija;
    @FXML private TableColumn<rezervacija, String> pocetak;
    @FXML private TableColumn<rezervacija, String> kraj;
    @FXML private TableColumn<rezervacija, Integer> idSala;
    @FXML private TableColumn<rezervacija, Integer> sifNastavnik;

    @FXML private TextField txt_idRezervacija;
    @FXML private TextField txt_nazivRezervacija;
    @FXML private DatePicker txt_datumRezervacija;
    @FXML private TextField txt_pocetak;
    @FXML private TextField txt_kraj;
    @FXML private TextField txt_idSala;
    @FXML private TextField txt_sifNastavnik;
    @FXML private TextField filterField;


    ObservableList<rezervacija> listM;
    ObservableList<rezervacija> dataList;

    int index = -1;

    Connection conn =null;
    ResultSet rs = null;
    PreparedStatement pst = null;

    private String username;

    public void initData(String uname){
        username = uname;
    }

    public void Add_rezervacija () {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String idRS = txt_idRezervacija.getText();
            int idnovi = Integer.parseInt(idRS);
            String nazivRezervacijanovi = txt_nazivRezervacija.getText();
            LocalDate datumR = txt_datumRezervacija.getValue();
            String datumnovi = datumR.toString();
            String pnovi = txt_pocetak.getText();
            String knovi = txt_pocetak.getText();
            String idSale = txt_idSala.getText();
            int idSnovi = Integer.parseInt(idSale);
            String sifN = txt_sifNastavnik.getText();
            int sifNnovi = Integer.parseInt(sifN);



            String upit = "INSERT INTO rezervacija(idRezervacija, nazivRezervacija, datRezervacija, pocetak, kraj, idSala, sifNastavnik) VALUES("
                    + idnovi  + ", '" + nazivRezervacijanovi + "', '" + datumnovi + "', '" + pnovi + "', '" + knovi + "', " + idSnovi + ", " + sifNnovi + ");";

            Statement statement = connection.createStatement();
            statement.execute(upit);

            JOptionPane.showMessageDialog(null, "Dodaj");
            UpdateTable();
            search_rezervacija();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //methode select rezervacija //
    @FXML
    void getSelected (MouseEvent event){
        index = table_rezervacija.getSelectionModel().getSelectedIndex();
        if (index <= -1){
            return;
        }

        txt_idRezervacija.setText(idRezervacija.getCellData(index).toString());
        txt_nazivRezervacija.setText(nazivRezervacija.getCellData(index).toString());
        ((TextField)txt_datumRezervacija.getEditor()).setText(datumRezervacija.getCellData(index).toString());
        txt_pocetak.setText(pocetak.getCellData(index).toString());
        txt_kraj.setText(kraj.getCellData(index).toString());
        txt_idSala.setText(idSala.getCellData(index).toString());
        txt_sifNastavnik.setText(sifNastavnik.getCellData(index).toString());

    }

    public void Edit () {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");
            String value1S = txt_idRezervacija.getText();
            int value1=Integer.parseInt(value1S);
            int v = 0;
            v=value1;
            String value2 = txt_nazivRezervacija.getText();
            LocalDate value3L = txt_datumRezervacija.getValue();
            String value3 = value3L.toString();
            String value4 = txt_pocetak.getText();
            String value5 = txt_kraj.getText();
            String value6S = txt_idSala.getText();
            String value7S = txt_sifNastavnik.getText();
            int value6=Integer.parseInt(value6S);
            int value7=Integer.parseInt(value7S);
            String upit = "UPDATE rezervacija set idRezervacija= "+value1+",nazivRezervacija= '"+value2+"',datRezervacija= '"+value3+"',pocetak= '"+value4+"',kraj= '"+value5+"',idSala= "+value6+",sifNastavnik= "+value7+" WHERE idRezervacija= "+v+" ";
            Statement statement = connection.createStatement();
            statement.execute(upit);

            JOptionPane.showMessageDialog(null, "Izmijeni");
            UpdateTable();
            search_rezervacija();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void Delete () {
        conn = mysqlconnect.ConnectDb();

        String sql = "DELETE FROM rezervacija WHERE idRezervacija = ?";
        try {
            pst = conn.prepareStatement(sql);
            pst.setString(1, txt_idRezervacija.getText());
            pst.execute();
            JOptionPane.showMessageDialog(null, "Izbrisi");
            UpdateTable();
            search_rezervacija();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }


    public void UpdateTable () {
        idRezervacija.setCellValueFactory(new PropertyValueFactory<>("idRezervacija"));
        nazivRezervacija.setCellValueFactory(new PropertyValueFactory<>("nazivRezervacija"));
        datumRezervacija.setCellValueFactory(new PropertyValueFactory<>("datumRezervacija"));
        pocetak.setCellValueFactory(new PropertyValueFactory<>("pocetak"));
        kraj.setCellValueFactory(new PropertyValueFactory<>("kraj"));
        idSala.setCellValueFactory(new PropertyValueFactory<>("idSala"));
        sifNastavnik.setCellValueFactory(new PropertyValueFactory<>("sifNastavnik"));

        listM = mysqlconnect.getDatarezervacije();
        table_rezervacija.setItems(listM);
    }

    @FXML
    void search_rezervacija () {
        idRezervacija.setCellValueFactory(new PropertyValueFactory<>("idRezervacija"));
        nazivRezervacija.setCellValueFactory(new PropertyValueFactory<>("nazivRezervacija"));
        datumRezervacija.setCellValueFactory(new PropertyValueFactory<>("datumRezervacija"));
        pocetak.setCellValueFactory(new PropertyValueFactory<>("pocetak"));
        kraj.setCellValueFactory(new PropertyValueFactory<>("kraj"));
        idSala.setCellValueFactory(new PropertyValueFactory<>("idSala"));
        sifNastavnik.setCellValueFactory(new PropertyValueFactory<>("sifNastavnik"));

        dataList = mysqlconnect.getDatarezervacije();
        table_rezervacija.setItems(dataList);
        FilteredList<rezervacija> filteredData = new FilteredList<>(dataList, b -> true);
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(rezervacija -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();

                if (rezervacija.getNazivRezervacija().toLowerCase().indexOf(lowerCaseFilter) != -1){
                    return true;
                }
                else if (String.valueOf(rezervacija.getIdSala()).indexOf(lowerCaseFilter) != -1) {
                    return true;
                } else if (String.valueOf(rezervacija.getSifNastavnik()).indexOf(lowerCaseFilter) != -1) {
                    return true;
                } else
                    return false;
            });
        });
        SortedList<rezervacija> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(table_rezervacija.comparatorProperty());
        table_rezervacija.setItems(sortedData);
    }

    @Override
    public void initialize (URL url, ResourceBundle rb){
        UpdateTable();
        search_rezervacija();
    }


    public static List<Stage> stagev = new ArrayList<Stage>();
    public void akcija(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText();;
        stagev.add((Stage) ((Node) actionEvent.getSource()).getScene().getWindow());
        System.out.println(text);
        if (text.equals("Nazad")) {
            nazad(actionEvent, username, "../sampleProfesor.fxml");
        }
    }
    public void nazad(ActionEvent actionEvent, String title, String path) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource(path));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle(title);
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
