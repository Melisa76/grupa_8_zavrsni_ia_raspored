
        package START.RASPORED;
        import javafx.event.ActionEvent;
        import javafx.fxml.FXMLLoader;
        import javafx.scene.Node;
        import javafx.scene.Parent;
        import javafx.scene.Scene;
        import javafx.scene.control.Label;
        import javafx.stage.Stage;
        import java.io.IOException;
        import java.sql.*;
        import java.time.LocalDate;
        import java.time.LocalDateTime;
        import java.time.LocalTime;
        import java.util.ArrayList;
        import java.util.List;

        import START.ENTITETI.*;
        import javax.xml.transform.Result;
public class RasporedController {
    private String username;
    private Connection con = null;
    private List<cas> casovi = new ArrayList<cas>();
    private Label lblError;

    private void getCasovi() throws SQLException {
        con = conDB();
        String query = "select predmet.nazPredmet, cas.tipCas, cas.pocetak, cas.kraj, cas.dan, grupa.nazGrupa from cas inner join predmet on cas.sifPredmet = predmet.sifPredmet inner join grupa on cas.idGrupa = grupa.idGrupa";
        Statement stmt = con.createStatement();
        ResultSet rows = stmt.executeQuery(query);
        if (!rows.isBeforeFirst()) {
            lblError.setText("Nemate casova za prikazati");
        } else {
            while (rows.next()){
                String nazPredmet = rows.getString("predmet.nazPredmet");
                String tipCas = rows.getString("cas.tipCas");
                String pocetak = rows.getString("cas.pocetak");
                String kraj = rows.getString("cas.kraj");
                String dan = rows.getString("cas.dan");
                String nazGrupa = rows.getString("grupanazGrupa");
                cas c = new cas(nazPredmet, tipCas, pocetak,kraj, dan, nazGrupa);
                casovi.add(c);
            }
        }
    }

    private static Connection conDB() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    //useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
                    "jdbc:mysql://localhost:3306/ia_raspored?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","asus"); // IZMJENE ZA DRUGE KORISNIKE
            System.out.println("Uspjesno povezivanje sa bazom podataka");
            return con;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleStart.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Neregistrovani korisnik");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}