package START;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class startController {

    public static List<Stage> stagev = new ArrayList<Stage>();
    public static List<String> fxmlv = new ArrayList<String>();

    public static Stage stage;

    public void Odaberi(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText();;
        stagev.add((Stage) ((Node) actionEvent.getSource()).getScene().getWindow());
        fxmlv.add("../START/sampleStart.fxml");

        System.out.println(text);
        if (text.equals("Prodekan/Profesor/Spoljni saradnik")) {
            akcija(actionEvent, "Prijava korisnika", "../START/LOGIN/sampleLogin.fxml");
        }

        else if (text.equals("Student/Neregistrovani korisnik")) {
            akcija(actionEvent, "Raspored", "../START/RASPORED/Rasporedsample.fxml");
        }


    }

    public void akcija(ActionEvent actionEvent, String title, String path) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource(path));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle(title);
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
