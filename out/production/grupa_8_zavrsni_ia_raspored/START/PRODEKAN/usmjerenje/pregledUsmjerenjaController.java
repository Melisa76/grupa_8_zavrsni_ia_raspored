package START.PRODEKAN.usmjerenje;

import START.ENTITETI.usmjerenje;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

public class pregledUsmjerenjaController implements Initializable {

    @FXML
    private TableView table;
    @FXML private TableColumn<usmjerenje, String> idUsmjerenje;
    @FXML private TableColumn<usmjerenje, String> oznUsmjerenje;
    @FXML private TableColumn<usmjerenje, String> nazivUsmjerenje;


    ObservableList<usmjerenje> usmjerenja = FXCollections.observableArrayList();

    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit1 = "SELECT * FROM usmjerenje";
            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery(upit1);

            while (resultSet1.next()) {
                usmjerenja.add(new usmjerenje(resultSet1.getInt(1), resultSet1.getString(2), resultSet1.getString(3)));
            }
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        idUsmjerenje.setCellValueFactory(new PropertyValueFactory<>("idUsmjerenje"));
        oznUsmjerenje.setCellValueFactory(new PropertyValueFactory<>("oznUsmjerenje"));
        nazivUsmjerenje.setCellValueFactory(new PropertyValueFactory<>("nazivUsmjerenje"));
        table.setItems(usmjerenja);
    }



    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("usmjerenje.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Dodajte usmjerenje");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
