package START.PRODEKAN.semestar;

import START.ENTITETI.semestar;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;


public class pregledSemestarController implements Initializable {
   @FXML private  TableView tableView;
   @FXML private  TableColumn<semestar, String> nazivSemestar;
   @FXML private TableColumn<semestar, String> oznSemestar;

    ObservableList<semestar> semestri = FXCollections.observableArrayList();

    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ia_raspored", "root", "!@bazepodataka1234");

            String upit = "SELECT * FROM semestar";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(upit);

            while(resultSet.next()){
                semestri.add(new semestar( resultSet.getString(1), resultSet.getString(2) ));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        nazivSemestar.setCellValueFactory(new PropertyValueFactory<>("nazivSemestar"));
        oznSemestar.setCellValueFactory(new PropertyValueFactory<>("oznSemestar"));
        tableView.setItems(semestri);
    }

    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleProdekan.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
