package START.PRODEKAN;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.Node;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProdekanController {

    public static List<Stage>stagev = new ArrayList<Stage>();
    public static List<String> fxmlv = new ArrayList<String>();

    public static Stage stage;

    public void DodajObjekat(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText();
        stagev.add((Stage) ((Node) actionEvent.getSource()).getScene().getWindow());
        fxmlv.add("../PRODEKAN/sampleProdekan.fxml");

        System.out.println(text);
        if (text.equals("Dodaj grupu")) {
            dodaj(actionEvent, "Dodajte grupu", "../PRODEKAN/grupa/grupa.fxml");
        }

        else if (text.equals("Dodaj čas")) {
            dodaj(actionEvent, "Dodajte cas", "../PRODEKAN/cas/cas.fxml");
        }

        else if (text.equals("Dodaj zgradu")) {
            dodaj(actionEvent, "Dodajte zgradu", "../PRODEKAN/zgrada/zgrada.fxml");
        }
        else if (text.equals("Dodaj salu")) {
            dodaj(actionEvent, "Dodajte salu", "../PRODEKAN/sala/sala.fxml");
        }

        else if (text.equals("Dodaj predmet")) {
            dodaj(actionEvent, "Dodajte predmet", "../PRODEKAN/predmet/predmet.fxml");
        }

        else if(text.equals("Semestar")){
            dodaj(actionEvent, "Pregled semestara", "../PRODEKAN/semestar/pregledSemestar.fxml");
        }

        else if (text.equals("Dodaj usmjerenje")) {
            dodaj(actionEvent, "Dodajte usmjerenje", "../PRODEKAN/usmjerenje/usmjerenje.fxml");
        }

        else if (text.equals("Dodaj uposlenika")) {
            dodaj(actionEvent, "Dodajte uposlenika", "../PRODEKAN/uposlenik/uposlenik.fxml");
        }

        else if (text.equals("Odjavite se")) {
            dodaj(actionEvent, "Dobro došli", "../sampleStart.fxml");
        }
    }

    public void dodaj(ActionEvent actionEvent, String title, String path) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource(path));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle(title);
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
