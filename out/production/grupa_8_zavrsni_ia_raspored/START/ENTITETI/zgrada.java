package START.ENTITETI;

public class zgrada {
    private int idZgrada;
    private String nazivZgrada;
    private String oznZgrada;

    public zgrada(int idZgrada, String nazivZgrada, String oznZgrada){
        this.idZgrada = idZgrada;
        this.nazivZgrada = nazivZgrada;
        this.oznZgrada = oznZgrada;
    }


    public String getNazivZgrada() {
        return nazivZgrada;
    }

    public String getOznZgrada() {
        return oznZgrada;
    }

    public void setOznZgrada(String oznZgrada) {
        this.oznZgrada = oznZgrada;
    }

    public void setNazivZgrada(String nazivZgrada) {
        this.nazivZgrada = nazivZgrada;
    }

    public int getIdZgrada() {
        return idZgrada;
    }

    public void setIdZgrada(int idZgrada) {
        this.idZgrada = idZgrada;
    }
}
