package START.ENTITETI;

public class nastavnik {
    private int sifNastavnik;
    private String imeNastavnik;
    private String prezNastavnik;
    private long  jmbgNastavnik;
    private String status;
    private boolean prodekan;
    private String username;
    private String password;

    public nastavnik(){}

    public nastavnik(int sifNastavnik, String imeNastavnik, String prezNastavnik,long  jmbgNastavnik,
           String status, boolean prodekan, String username, String password){
        this.sifNastavnik = sifNastavnik;
        this.imeNastavnik = imeNastavnik;
        this.prezNastavnik = prezNastavnik;
        this.jmbgNastavnik = jmbgNastavnik;
        this.status = status;
        this.prodekan = prodekan;
        this.username = username;
        this.password = password;
    }

    public int getSifNastavnik() {
        return sifNastavnik;
    }

    public void setSifNastavnik(int sifNastavnik) {
        this.sifNastavnik = sifNastavnik;
    }

    public String getImeNastavnik() {
        return imeNastavnik;
    }

    public void setImeNastavnik(String imeNastavnik) {
        this.imeNastavnik = imeNastavnik;
    }

    public String getPrezNastavnik() {
        return prezNastavnik;
    }

    public void setPrezNastavnik(String prezNastavnik) {
        this.prezNastavnik = prezNastavnik;
    }

    public long getJmbgNastavnik() {
        return jmbgNastavnik;
    }

    public void setJmbgNastavnik(long jmbgNastavnik) {
        this.jmbgNastavnik = jmbgNastavnik;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isProdekan() {
        return prodekan;
    }

    public void setProdekan(boolean prodekan) {
        this.prodekan = prodekan;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
